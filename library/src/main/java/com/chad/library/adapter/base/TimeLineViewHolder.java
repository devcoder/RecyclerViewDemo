package com.chad.library.adapter.base;

import android.support.annotation.IdRes;
import android.support.v7.widget.RecyclerView;
import android.util.SparseArray;
import android.view.View;
import android.widget.TextView;


/**
 * https://github.com/CymChad/BaseRecyclerViewAdapterHelper
 */
public class TimeLineViewHolder extends RecyclerView.ViewHolder {

    /**
     * Views indexed with their IDs
     */
    private final SparseArray<View> views;


    public TimeLineViewHolder(final View view) {
        super(view);
        this.views = new SparseArray<>();
    }



    @SuppressWarnings("unchecked")
    public <T extends View> T getView(@IdRes int viewId) {
        View view = views.get(viewId);
        if (view == null) {
            view = itemView.findViewById(viewId);
            views.put(viewId, view);
        }
        return (T) view;
    }

    public void setText(int tv, String text) {
        TextView textView = itemView.findViewById(tv);
        textView.setText(text);
    }
}
